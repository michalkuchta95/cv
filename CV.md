# CURRICULUM VITAE
Dane osobowe 
Imię i nazwisko:  **Michał Kuchta**
Data urodzenia: 09.07.1995 
Adres: ul. Młynki 111, 24-320 Poniatowa 
Telefon: 607 369 764   
E-mail: michal.kuchta95@gmail.com 
 
***  Zmiana w bitbucket ***
 
### Wykształcenie 
* 2011 – 2014  ZS Liceum Ogólnokształcące im. Marszałka Józefa Piłsudskiego w Poniatowej; 
* 2014 – 2017 Uniwersytet Marii-Curie Skłodowskiej w Lublinie. Kierunek: Geoinformatyka Uzyskany tytuł: Licencjat 
* 2018 – dzisiaj Uniwersytet Marii-Curie Skłodowskiej w Lublinie. Kierunek: Geoinformatyka II stopnia 
### Doświadczenie zawodowe 
* 08/2016 – 09/2016 Praktykant w Starostwie Powiatowym w Opolu Lubelskim. 
### Dodatkowe informacje 
* Języki: Język angielski: B2 
Inne : 
* Prawo jazdy kategorii B; 
* Bardzo dobra znajomość obsługi komputera w środowisku Windows;  
* Bardzo dobra znajomość pakietu MS-Office; 
* Bardzo dobra znajomość aplikacji Qgis i Arcmap; 
* Podstawowa znajomość języku programowania Python, C++, Java; 
* Podstawowa znajomość Git, GitHub; 
* Znajomość HTML, CSS i JavaScript; 
* Umiejętność pracy w zespole. 
 
### Zainteresowania i hobby 
muzyka, zaawansowane technologie, gastronomia, motoryzacja, gry komputerowe. 


 Wyrażam zgodę na przetwarzanie danych osobowych zawartych w niniejszym dokumencie do realizacji procesu rekrutacji zgodnie z ustawą z dnia 10 maja 2018 roku o ochronie danych osobowych (Dz. Ustaw z 2018, poz. 1000) oraz zgodnie z Rozporządzeniem Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (RODO).  