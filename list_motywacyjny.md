W związku z poszukiwaniem przez Państwa pracownika na stanowisko XXXX przesyłam swoją aplikację – to zdanie zdecydowanie usuń. Każdy rekruter przeczytał ją już jakieś 15.000 razy.


W związku z ogłoszeniem zamieszczony w serwisie pracy XXXX przesyłam CV oraz list motywacyjny – obecnie większość firm posiada specjalne formularze rekrutacyjne śledzące źródło kandydatury. Nie musisz już pisać, gdzie znalazłeś ofertę pracy (zwłaszcza, że często pojawiają się tu błędne oznaczenia).


Uprzejmie proszę o przyjęcie mnie do pracy w Państwa firmie. Swoją prośbę motywuję(…) List motywacyjny ma otwierać dialog. Nie jest podaniem o pracę ani petycją..